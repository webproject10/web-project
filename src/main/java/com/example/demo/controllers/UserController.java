package com.example.demo.controllers;

import com.example.demo.entities.User;
import com.example.demo.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URLEncoder;

import java.util.List;

@RestController
public class UserController {

    @Autowired
    private UserService userService;



    /*** Create - Add a new account
     *
     * @param user object command
     * @return The user object saved
     */
    @PostMapping("/signIn")
    public ResponseEntity<User> register(@RequestBody User user) {
        User _user= userService.registerUser(user);
             return new ResponseEntity<>(_user,HttpStatus.CREATED);

    }



    /**
     * Create - LOgin
     *
     * @param email object user
     * @param password object user
     * @return The user object saved
     */
    @PostMapping("/login")
    public ResponseEntity<String> login(@RequestParam("email") String email ,@RequestParam("password") String password) {
        User _user = userService.login(email, password);
        if (_user != null)
            return new ResponseEntity<>("Login Success", HttpStatus.CREATED);

        return new ResponseEntity<>("Sorry password or email invalid", HttpStatus.NOT_FOUND);
    }
    /**
     * Create - LOg Out
     *
     * @param email object String
     * @param password object String
     * @return The user object saved
     */
    @PostMapping("/logout")
    public ResponseEntity<User> logout(@RequestParam("email") String email ,@RequestParam("password") String password) {
        User _user= userService.logOut(email,password);
        return new ResponseEntity<>(_user, HttpStatus.CREATED);

    }

    /**
     * Read - Get all users
     *
     * @return - An Iterable object of user full filled
     */
    @GetMapping("/users")
    public ResponseEntity<List<User>> getUsers() {

        if (userService.getUsers().isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }

        return new ResponseEntity<>(userService.getUsers(), HttpStatus.OK);
    }


    /**
     * Delete - Delete all users
     */
    @DeleteMapping("/users")
    public ResponseEntity<HttpStatus> deleteAllUsers() {
        userService.deleteAllUsers();
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);

    }



}

package com.example.demo.controllers;

import com.example.demo.entities.Commande;
import com.example.demo.entities.Livraison;
import com.example.demo.exception.ResourceNotFoundException;
import com.example.demo.services.LivraisonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
public class LivraisonController {
    @Autowired
    private LivraisonService  livraisonService;

    /**
     * Create - Add a new livraison
     *
     * @param livraison object command
     * @return The command object saved
     */
    @PostMapping("/livraison")
    public ResponseEntity<Livraison> addLivraison(@RequestBody Livraison livraison) {
        Livraison _livraison = livraisonService.addLivraison(livraison);
        return new ResponseEntity<>(_livraison, HttpStatus.CREATED);

    }

    /**
     * Read - Get one livraison
     *
     * @param id The id of the livraison
     * @return An Livraison object full filled
     */
    @GetMapping("/livraison/{id}")
    public ResponseEntity<Livraison> getLivraison(@PathVariable("id") final Long id) {
        Livraison livraison = livraisonService.getLivraison(id)
                .orElseThrow(() -> new ResourceNotFoundException("Not found Lvraison with id = " + id));

        return new ResponseEntity<>(livraison, HttpStatus.OK);
    }

    /**
     * Read - Get all livraisons
     *
     * @return - An Iterable object of Client full filled
     */
    @GetMapping("/livraisons")
    public ResponseEntity<List<Livraison>> getLivraison() {

        if (livraisonService.getLivraisons().isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }

        return new ResponseEntity<>(livraisonService.getLivraisons(), HttpStatus.OK);
    }

    /**
     * Update - Update an existing Livraison
     *
     * @param id      - The id of the Livraison to update
     * @param livraison- The Livraison object updated
     * @return
     */
    @PutMapping("/livraison/{id}")
    public ResponseEntity<Livraison> updateLivraison(@PathVariable("id") final Long id, @RequestBody Livraison livraison) {
        Livraison currentLivraison = livraisonService.getLivraison(id)
                .orElseThrow(() -> new ResourceNotFoundException("Not found Livraison with id = " + id));

        Long numLivraison = livraison.getNumLivraison();
        if (numLivraison != null) {
            currentLivraison.setNumLivraison(numLivraison);
        }
        String date = livraison.getDateLivraison();
        if (date != null) {
            currentLivraison.setDateLivraison(date);
        }
	 	Commande commande = livraison.getCommande();
		if (commande != null) {
			currentLivraison.setCommande(commande);
		}


        return new ResponseEntity<>(livraisonService.addLivraison(currentLivraison), HttpStatus.OK);
    }

    /**
     * Delete - Delete an Livraison
     *
     * @param id - The id of the LIvraison to delete
     */
    @DeleteMapping("/livraison/{id}")
    public ResponseEntity<HttpStatus> deleteCommande(@PathVariable("id") final Long id) {
        livraisonService.deleteLivraison(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);

    }

    /**
     * Delete - Delete all Livraison
     */
    @DeleteMapping("/livraisons")
    public ResponseEntity<HttpStatus> deleteAllClients() {
        livraisonService.deleteAllLivraison();
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);

    }

    /**
     * Read - Get all livraisons
     *
     * @param date The date of the client
     * @return An livraison object full filled
     */
    @GetMapping("/livraisons/date/{date}")
    public ResponseEntity<List<Livraison>> getByDate(@PathVariable("date") final String date) {
        if (livraisonService.getByDate(date).isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }

        return new ResponseEntity<>(livraisonService.getByDate(date), HttpStatus.OK);
    }


}

package com.example.demo.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.example.demo.entities.Client;
import com.example.demo.exception.ResourceNotFoundException;
import com.example.demo.services.ClientService;
@CrossOrigin("*")
@RestController

public class ClientController {
	  @Autowired
      private ClientService clientService;

      /**
       * Create - Add a new client
       * @param client object client
       * @return The client  object saved
       */
      @PostMapping("/client")
      public ResponseEntity<Client> createClient(@RequestBody Client client) {
         Client _client = clientService.addClient(client);
          return new ResponseEntity<>(_client, HttpStatus.CREATED);

      }


      /**
       * Read - Get one client
       * @param id The id of the client
       * @return An Client object full filled
       */
      @GetMapping("/client/{id}")
      public ResponseEntity<Client> getClient(@PathVariable("id") final Integer id) {
              Client client = clientService.getClient(id)
                   .orElseThrow(() -> new ResourceNotFoundException("Not found Client with id = " + id));

              return new ResponseEntity<>(client, HttpStatus.OK);
      }

      /**
       * Read - Get all clients
       * @return - An Iterable object of Client full filled
       */
      @GetMapping("/clients")
      public ResponseEntity<List<Client> > getClients() {

          if (clientService.getClients().isEmpty()) {
              return new ResponseEntity<>(HttpStatus.NO_CONTENT);
          }

          return new ResponseEntity<>(clientService.getClients(), HttpStatus.OK);
      }


      /**
       * Update - Update an existing client
       * @param id - The id of the client to update
       * @param client- The client object updated
       * @return
       */
      @PutMapping("/client/{id}")
      public ResponseEntity<Client> updateClient(@PathVariable("id") final Integer id, @RequestBody Client client) {
               Client currentClient = clientService.getClient(id)
                  .orElseThrow(() -> new ResourceNotFoundException("Not found Client with id = " + id));

               String adresse = client.getAdresse();
              if(adresse!= null) {
            	  currentClient.setAdresse(adresse);
              }
              String mail = client.getMail();
              if(mail != null) {
            	  currentClient.setMail(mail);
              }
          	  String nom = client.getNom();
              if(nom != null) {
            	  currentClient.setNom(nom);
              }
              String prenom = client.getPrenom();
              if(prenom!= null) {
            	  currentClient.setPrenom(prenom);
              }

         return new ResponseEntity<>(clientService.addClient(currentClient), HttpStatus.OK);
      }


      /**
       * Delete - Delete an client
       * @param id - The id of the client to delete
       */
      @DeleteMapping("/client/{id}")
      public ResponseEntity<HttpStatus> deleteClient(@PathVariable("id") final Integer id) {
          clientService.deleteClient(id);
          return new ResponseEntity<>(HttpStatus.NO_CONTENT);

      }

      /**
       * Delete - Delete all clients
       */
      @DeleteMapping("/clients")
      public ResponseEntity<HttpStatus> deleteAllClients() {
    	  clientService.deleteAllClient();
          return new ResponseEntity<>(HttpStatus.NO_CONTENT);

      }


      /**
       * Read - Get all clients
       * @param nom The nom of the client
       * @return An Client object full filled
       */
      @GetMapping("/clients/nom/{nom}")
      public ResponseEntity<List<Client>> getByNom(@PathVariable("nom") final String nom) {
          if (clientService.getByNom(nom).isEmpty()) {
              return new ResponseEntity<>(HttpStatus.NO_CONTENT);
          }

          return new ResponseEntity<>(clientService.getByNom(nom), HttpStatus.OK);
      }

      /**
       * Read - Get all client
       * @param Nom The nom and Prenom of the client
       * @return An Client object full filled
       */
      @GetMapping("/clients/nomAndPrenom/{nom}/{prenom}")
      public ResponseEntity <List<Client>>  getClientByNomAndPrenom(@PathVariable("nom") final String nom,@PathVariable("prenom") final String prenom) {
          if (clientService.getByNomAndPrenom(nom, prenom).isEmpty()) {
              return new ResponseEntity<>(HttpStatus.NO_CONTENT);
          }

      return new ResponseEntity<>(clientService.getByNomAndPrenom(nom, prenom), HttpStatus.OK);
  }

}

package com.example.demo.controllers;
import com.example.demo.entities.Commande;
import com.example.demo.exception.ResourceNotFoundException;
import com.example.demo.services.ArticleService;
import com.example.demo.services.CommandeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import com.example.demo.entities.Article;
import java.util.List;

import org.springframework.web.bind.annotation.RestController;

@RestController
public class ArticlesController {

        @Autowired
        private ArticleService articleService;

        @Autowired
        private CommandeService commandeService;

        /**
         * Create - Add a new article
         * @param article object article
         * @return The article  object saved
         */
        @PostMapping("/article")
        public ResponseEntity<Article> createArticle(@RequestBody Article article) {
           Article _article = articleService.saveArticle(article);
            return new ResponseEntity<>(_article, HttpStatus.CREATED);

        }


        /**
         * Read - Get one article
         * @param id The id of the article
         * @return An Article object full filled
         */
        @GetMapping("/article/{id}")
        public ResponseEntity<Article> getArticle(@PathVariable("id") final Long id) {
                Article article = articleService.getArticle(id)
                     .orElseThrow(() -> new ResourceNotFoundException("Not found Article with id = " + id));

                return new ResponseEntity<>(article, HttpStatus.OK);
        }

        /**
         * Read - Get all articles
         * @return - An Iterable object of Article full filled
         */
        @GetMapping("/articles")
        public ResponseEntity<List<Article> > getArticles() {

            if (articleService.getArticles().isEmpty()) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }

            return new ResponseEntity<>(articleService.getArticles(), HttpStatus.OK);
        }


        /**
         * Update - Update an existing article
         * @param id - The id of the article to update
         * @param article- The article object updated
         * @return
         */
        @PutMapping("/article/{id}")
        public ResponseEntity<Article> updateArticle(@PathVariable("id") final Long id, @RequestBody Article article) {
                 Article currentArticle = articleService.getArticle(id)
                    .orElseThrow(() -> new ResourceNotFoundException("Not found Article with id = " + id));

                String categorie = article.getCategorie();
                if(categorie!= null) {
                    currentArticle.setCategorie(categorie);
                }
                String libele = article.getLibele();
                if(libele != null) {
                    currentArticle.setLibele(libele);;
                }
                Long codeArticle = article.getCodeArticle();
                if(codeArticle != null) {
                    currentArticle.setCodeArticle(codeArticle);
                }
                Float prix = article.getPrix();
                if(prix!= null) {
                    currentArticle.setPrix(prix);;
                }

                Long quantite = article.getQuantite();
                if(quantite!= null) {
                    currentArticle.setQuantite(quantite);;
                }


            return new ResponseEntity<>(articleService.saveArticle(currentArticle), HttpStatus.OK);
        }



        /**
         * Add Article To commande - Update an existing article
         * @param id - The id of the article to update
         * @param article- The article object updated
         * @return
         */
        @PutMapping("/articleCommande/{id}")
        public ResponseEntity<Article> addArticleToCommande(@PathVariable("id") final Long id, @RequestBody Article article) {
            Article currentArticle = articleService.getArticle(id)
                    .orElseThrow(() -> new ResourceNotFoundException("Not found Article with id = " + id));


            Commande commande = article.getCommande();
            if(commande!= null) {
                currentArticle.setCommande(commande);
                currentArticle.setQuantite(currentArticle.getQuantite()-1);
            }

            return new ResponseEntity<>(articleService.saveArticle(currentArticle), HttpStatus.OK);
        }


        /**
         * Delete - Delete an article
         * @param id - The id of the article to delete
         */
        @DeleteMapping("/article/{id}")
        public ResponseEntity<HttpStatus> deleteArticle(@PathVariable("id") final Long id) {
            articleService.deleteArticle(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);

        }

        /**
         * Delete - Delete all article
         */
        @DeleteMapping("/articles")
        public ResponseEntity<HttpStatus> deleteAllArticle() {
            articleService.deleteAllArticle();
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);

        }


        /**
         * Read - Get all article
         * @param libele The libele of the article
         * @return An Article object full filled
         */
        @GetMapping("/articles/libele/{libele}")
        public ResponseEntity<List<Article>> getArticleByLibele(@PathVariable("libele") final String libele) {
            if (articleService.getByLibele(libele).isEmpty()) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }

            return new ResponseEntity<>(articleService.getByLibele(libele), HttpStatus.OK);
        }

        /**
         * Read - Get all article
         * @param libele The libele and Categorie of the article
         * @return An Article object full filled
         */
        @GetMapping("/articles/libeleAndCategorie/{libele}/{categorie}")
        public ResponseEntity<List<Article>> getArticleByLibeleAndCategorie(@PathVariable("libele") final String libele,@PathVariable("categorie") final String categorie) {
            if (articleService.getByLibeleAndCategorie(libele,categorie).isEmpty()) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }

        return new ResponseEntity<>(articleService.getByLibeleAndCategorie(libele,categorie), HttpStatus.OK);
    }


}
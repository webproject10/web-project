package com.example.demo.controllers;

import java.util.List;

import com.example.demo.entities.Livraison;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.entities.Client;
import com.example.demo.entities.Commande;
import com.example.demo.exception.ResourceNotFoundException;
import com.example.demo.services.CommandeService;

@RestController
public class CommandeController {
	@Autowired
	private CommandeService commandeService;

	/**
	 * Create - Add a new command
	 * 
	 * @param commande object command
	 * @return The command object saved
	 */
	@PostMapping("/commande")
	public ResponseEntity<Commande> addCommande(@RequestBody Commande commande) {
		Commande _commande = commandeService.addCommande(commande);
		return new ResponseEntity<>(_commande, HttpStatus.CREATED);

	}

	/**
	 * Read - Get one command
	 * 
	 * @param id The id of the command
	 * @return An Command object full filled
	 */
	@GetMapping("/commande/{id}")
	public ResponseEntity<Commande> getCommande(@PathVariable("id") final Long id) {
		Commande commande = commandeService.getCommande(id)
				.orElseThrow(() -> new ResourceNotFoundException("Not found Commande with id = " + id));

		return new ResponseEntity<>(commande, HttpStatus.OK);
	}

	/**
	 * Read - Get all commandes
	 * 
	 * @return - An Iterable object of Client full filled
	 */
	@GetMapping("/commandes")
	public ResponseEntity<List<Commande>> getCommandes() {

		if (commandeService.getCommandes().isEmpty()) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}

		return new ResponseEntity<>(commandeService.getCommandes(), HttpStatus.OK);
	}

	/**
	 * Update - Update an existing Commande
	 * 
	 * @param id      - The id of the Commande to update
	 * @param commande- The Commande object updated
	 * @return
	 */
	@PutMapping("/commande/{id}")
	public ResponseEntity<Commande> updateCommande(@PathVariable("id") final Long id, @RequestBody Commande commande) {
		Commande currentCommande = commandeService.getCommande(id)
				.orElseThrow(() -> new ResourceNotFoundException("Not found Commande with id = " + id));

		String codee = commande.getCodee();
		if (codee != null) {
			currentCommande.setCodee(codee);
		}
		String date = commande.getDate();
		if (date != null) {
			currentCommande.setDate(date);
		}
		Client client = commande.getClient();
		if (client != null) {
			currentCommande.setClient(client);
		}
		Livraison livraison = commande.getLivraison();
		if (livraison != null) {
			currentCommande.setLivraison(livraison);
		}


		return new ResponseEntity<>(commandeService.addCommande(currentCommande), HttpStatus.OK);
	}

	/**
	 * Delete - Delete an Commande
	 * 
	 * @param id - The id of the Commande to delete
	 */
	@DeleteMapping("/commande/{id}")
	public ResponseEntity<HttpStatus> deleteCommande(@PathVariable("id") final Long id) {
		commandeService.deleteCommande(id);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);

	}

	/**
	 * Delete - Delete all commande
	 */
	@DeleteMapping("/commandes")
	public ResponseEntity<HttpStatus> deleteAllClients() {
		commandeService.deleteAllCommande();
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);

	}

	/**
	 * Read - Get all commandes
	 * 
	 * @param date The date of the client
	 * @return An commande object full filled
	 */
	@GetMapping("/commandes/date/{date}")
	public ResponseEntity<List<Commande>> getByDate(@PathVariable("date") final String date) {
		if (commandeService.getByDate(date).isEmpty()) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}

		return new ResponseEntity<>(commandeService.getByDate(date), HttpStatus.OK);
	}


}

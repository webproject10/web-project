package com.example.demo;

import com.example.demo.controllers.LivraisonController;
import com.example.demo.repositories.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import com.example.demo.repositories.LivraisonRepository;


@SpringBootApplication
@EnableAspectJAutoProxy(proxyTargetClass = true)
public class SpringProjectApplication implements CommandLineRunner {

	@Autowired
	private ArticleRepository articleRepository;
	@Autowired
	private ClientRepository clientRepository;

	@Autowired
	private CommandeRepository commandeRepository;

	@Autowired
	private LivraisonRepository livraisonRepository;

	public static void main(String[] args) {
		SpringApplication.run(SpringProjectApplication.class, args);

	}

	@Override
	public void run(String... args) throws Exception {




	}
}


package com.example.demo.entities;
import java.util.Collection;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;


@Data
@NoArgsConstructor
@ToString
@Entity
@Table(name="commande")
public class Commande {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "idCommande")
	private Long idCommande;

	@Column(name = "codee")
	private String codee;

	@Column(name = "date")
	private String date;


	@JsonManagedReference
	@OneToMany(mappedBy = "commande")
	private Collection<Article> articles;


	@ManyToOne()
	@JoinColumn(name = "idC")
	@JsonBackReference
	private Client client;

	/*ToOne is FetchType.EAGER by default*/
	@NotFound(action = NotFoundAction.IGNORE)
	@JsonManagedReference
	@OneToOne()
	@JoinColumn(name = "numLivraison", referencedColumnName = "numLivraison")
	private Livraison livraison;


	public Commande(Long idCommande, String codee, String date, Client client) {
		this.idCommande = idCommande;
		this.codee = codee;
		this.date = date;
		this.client = client;
	}
}

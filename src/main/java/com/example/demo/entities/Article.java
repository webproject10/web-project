package com.example.demo.entities;

import java.util.Collection;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.*;

@Entity
@Table(name="article")
@Data @ToString @NoArgsConstructor
public class Article {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="code")
	private Long codeArticle;
	private String libele;
	private Float prix;
	private String categorie;
	private Long quantite;


	@NotFound(action = NotFoundAction.IGNORE)
	@ManyToOne()
	@JoinColumn(name="idCommande")
	@JsonBackReference
	private Commande commande;


	public Article(Long codeArticle,String libele,float prix,String categorie,long quantite,Commande commande){
		this.codeArticle=codeArticle;
		this.libele=libele;
		this.prix=prix;
		this.categorie=categorie;
		this.quantite=quantite;
		this.commande=commande;
	}
	}




package com.example.demo.entities;

import java.util.Collection;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Entity
@Table(name = "client")
public class Client {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="idC")
	private Integer idC;
	
	@Column(name="adresse")
	private String adresse;
	
	@Column(name="mail")
	private String mail;
	
	@Column(name="nom")
	private String nom;
	
	@Column(name="prenom")
	private String prenom;

	@JsonManagedReference
	@OneToMany(mappedBy ="client")
	private Collection<Commande> commandes;

	public Client(String adresse,String mail,String nom,String prenom){
	this.adresse=adresse;
	this.mail=mail;
	this.nom=nom;
	this.prenom=prenom;
}

}

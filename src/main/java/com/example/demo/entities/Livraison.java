
package com.example.demo.entities;
import java.util.Date;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;


@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Entity
@Table(name="livraison")
public class Livraison {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="numLivraison")
	private Long numLivraison;

	@Column(name="dateLivraison")
	private String dateLivraison;

	@OneToOne()
	@JoinColumn(name = "idCommande")
	@JsonBackReference
	private Commande commande;

}


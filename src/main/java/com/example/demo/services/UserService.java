package com.example.demo.services;


import com.example.demo.entities.Commande;
import com.example.demo.entities.User;
import com.example.demo.repositories.UserRepository;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

@Data
@Service
public class UserService {
    @Autowired
    private UserRepository userRepository;
    public List<User> getUsers() {
        return userRepository.findAll();
    }

    public User registerUser( User newUser) {
        List<User> users = userRepository.findAll();
        System.out.println("New user: " + newUser.toString());
        for (User user : users) {
            System.out.println("Registered user: " + newUser.toString());
            if (user.equals(newUser)) {
                System.out.println("User Already exists!");
            }
        }
        userRepository.save(newUser);
        return newUser;
    }


    public User login(String email, String password) {
        User user = userRepository.findUserByEmailAndPassword(email, password);
        user.setLoggedIn(true);
        return user;
    }


    public User logOut(String email, String password) {
        User user = userRepository.findUserByEmailAndPassword(email, password);
        user.setLoggedIn(false);
        return user;
    }

    public void deleteAllUsers() { userRepository.deleteAll(); }



}

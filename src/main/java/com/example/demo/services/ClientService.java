package com.example.demo.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.entities.Client;
import com.example.demo.repositories.ClientRepository;

import lombok.Data;

@Data
@Service
public class ClientService {
	@Autowired
	private ClientRepository clientRepository;

	public List<Client> getClients() {
		return clientRepository.findAll();
	}

	public Optional<Client> getClient(final Integer id) {
		return clientRepository.findById(id);
	}

	public Client addClient(Client client) {
		Client saveClient = clientRepository.save(client);
		return saveClient;
	}

	public void deleteClient(final Integer id) {
		clientRepository.deleteById(id);
	}

	public void deleteAllClient() {
		clientRepository.deleteAll();
	}

	public  List<Client> getByNom(final String name) {
		return clientRepository.findByNomContains(name);
	}

	public List<Client> getByNomAndPrenom(final String nom, final String prenom) {
		return clientRepository.findByNomContainsAndPrenomContains(nom, prenom);
	}

}

package com.example.demo.services;

import com.example.demo.entities.Article;
import com.example.demo.repositories.ArticleRepository;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Optional;

@Data
@Service
public class ArticleService {

    @Autowired
    private ArticleRepository articleRepository;


    public List<Article> getArticles() {
        return articleRepository.findAll();
    }

    public Optional<Article> getArticle(final Long id) {
        return articleRepository.findById(id);
    }

    public Article saveArticle(Article article) {
        Article savedArticle = articleRepository.save(article);
        return savedArticle;
    }


    public void deleteArticle(final Long id) {
        articleRepository.deleteById(id);
    }

    public void deleteAllArticle() { articleRepository.deleteAll(); }

    public List<Article> getByLibele(final String nom){return articleRepository.findByLibeleContains(nom);}

    public List<Article> getByLibeleAndCategorie(final String nom,final String cat){return articleRepository.findByLibeleContainsAndCategorieContains(nom,cat);}






}

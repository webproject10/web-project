package com.example.demo.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.entities.Commande;
import com.example.demo.repositories.CommandeRepository;

import lombok.Data;

@Data
@Service
public class CommandeService {
	    @Autowired
	    private CommandeRepository commandeRepository;


	    public List<Commande> getCommandes() {
	        return commandeRepository.findAll();
	    }

	    public Optional<Commande> getCommande(final Long id) {
	        return commandeRepository.findById(id);
	    }

	    public Commande addCommande(Commande commande) {
	    	Commande savedCommande = commandeRepository.save(commande);
	        return savedCommande;
	    }

	    public void deleteCommande(final Long id) {
	    	commandeRepository.deleteById(id);
	    }

	    public void deleteAllCommande() { commandeRepository.deleteAll(); }

	    public List<Commande> getByDate(final String date){return commandeRepository.findByDate(date);}



}

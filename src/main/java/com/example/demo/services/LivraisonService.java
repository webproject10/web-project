package com.example.demo.services;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import com.example.demo.entities.Livraison;
import com.example.demo.repositories.LivraisonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import lombok.Data;



@Data
@Service
public class LivraisonService {
    @Autowired
    private LivraisonRepository livraisonRepository;


    public List<Livraison> getLivraisons() {
        return livraisonRepository.findAll();
    }

    public Optional<Livraison> getLivraison(final Long id) {
        return livraisonRepository.findById(id);
    }

    public Livraison addLivraison(Livraison livraison) {
        Livraison savedLivraison=  livraisonRepository.save(livraison);
        return savedLivraison;
    }

    public void deleteLivraison(final Long id) {
        livraisonRepository.deleteById(id);
    }

    public void deleteAllLivraison() { livraisonRepository.deleteAll(); }

    public List<Livraison> getByDate(final String date){return   livraisonRepository.findByDateLivraison(date);}



}
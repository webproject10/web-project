
package com.example.demo.repositories;
import com.example.demo.entities.Livraison;
import org.springframework.data.jpa.repository.JpaRepository;
import java.util.List;

public interface LivraisonRepository extends JpaRepository <Livraison, Long> {

    List<Livraison> findByDateLivraison(String date);


}

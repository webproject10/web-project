package com.example.demo.repositories;
import com.example.demo.entities.Commande;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CommandeRepository extends JpaRepository <Commande, Long> {
    List<Commande> findByDate(String date);
}

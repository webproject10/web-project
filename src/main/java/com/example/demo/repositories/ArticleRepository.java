package com.example.demo.repositories;
import com.example.demo.entities.Article;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface ArticleRepository extends JpaRepository <Article, Long> {

    List<Article> findByLibeleContains(String nom);
    List<Article> findByLibeleContainsAndCategorieContains(String nom,String cat);

   /* @Query("select p from Article p where p.etat like :x ")
    public List<Article> chercher(@Param("x")String etat);*/

}

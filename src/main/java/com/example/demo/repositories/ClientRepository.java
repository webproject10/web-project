package com.example.demo.repositories;
import com.example.demo.entities.Client;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;


public interface ClientRepository extends JpaRepository <Client, Integer> {
	
    List<Client> findByNomContains(String nom);
    List<Client> findByNomContainsAndPrenomContains(String nom,String prenom);


}

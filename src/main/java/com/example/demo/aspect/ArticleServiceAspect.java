package com.example.demo.aspect;
import com.example.demo.entities.Article;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

@Aspect
public class ArticleServiceAspect {
    @Before(value = "execution(* com.example.demo.services.ArticleService.*(..)) and args(article)")
    public void beforeAdvice(JoinPoint joinPoint, Article article) {
        System.out.println("Before method:" + joinPoint.getSignature());

        System.out.println("Creating Article with libele - " + article.getLibele() + " and categorie - " + article.getCategorie());
    }

    @After(value = "execution(* com.example.demo.services.ArticleService.*(..)) and args(article)")
    public void afterAdvice(JoinPoint joinPoint, Article article) {
        System.out.println("After method:" + joinPoint.getSignature());

        System.out.println("Successfully created Article with libele - " + article.getLibele() + " and categorie - " + article.getCategorie());

    }

}

